#!/usr/bin/env python
from __future__ import unicode_literals
import argparse
import logging

from domotz_stress.app import StressApp


def main(args):
    app = StressApp(arguments=args)
    app.start()


if __name__ == '__main__':
    logging.basicConfig()
    logger = logging.getLogger()
    parser = argparse.ArgumentParser(description=
                                     'Creates a set of fake URC agents and sends commands to the cloud simulating the '
                                     'behaviour')
    parser.add_argument('-e', '--endpoint', type=str, default='http://127.0.0.1:9080',
                        help="If present, the base URL of the stressed cloud, defaults to localhost:9080 - e.g. "
                             "https://api-staging-eu-central-1-cell-1.domotz.co/")
    parser.add_argument('-s', '--secret', type=str, default='',
                        help="The shared secret for agent creation - e.g. ''")
    parser.add_argument('-n', '--vendor-name', type=str, default='URC',
                        help="The vendor name - default URC")
    parser.add_argument('-t', '--agent-type', type=str, default='URC',
                        help="The agent type - default URC")
    parser.add_argument('-k', '--api-key', type=str, default='',
                        help="The api-key name")
    parser.add_argument('-N', '--agent-number', type=int, default=1,
                        help="Number of ACTIVE agents to be created")
    parser.add_argument('-W', '--standby-agent-number', type=int, default=0,
                        help="Number of STANDBY agents to be created")
    parser.add_argument('-D', '--devices-number', type=int, default=10,
                        help="Number of devices per agent (max 200)")
    parser.add_argument('-m', '--base-mac', type=str, default='00:00',
                        help="Base MAC address for agents")
    parser.add_argument('-S', '--device-stability', type=int, default=99,
                        help="'Stability' of the devices - every device has this value as %% for keeping "
                             "the same status every discovery")
    parser.add_argument('-v', '--verbose', action='store_true',
                        help="Set verbose output on stderr")
    args = parser.parse_args()
    if args.verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    for handler in logging.getLogger().handlers:
        handler.setFormatter(formatter)
    if args.agent_number > 65535:
        raise RuntimeError("Too many agents")

    main(args)
