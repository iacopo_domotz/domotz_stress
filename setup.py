#!/usr/bin/env python
from os.path import dirname
import os
import sys

from setuptools import setup
from setuptools import find_packages

base_dir = os.path.join(dirname(__file__), 'src')
sys.path.insert(0, base_dir)

import domotz_stress
from domotz_stress import PROJECT_NAME

with open('requirements.txt') as req_files:
    required_list = req_files.read()

    packages = find_packages(where=base_dir)

    setup(
        name=PROJECT_NAME,
        version=os.environ.get('DOMOTZ_VERSION') or domotz_stress.__version__,
        author="Iacopo Papalini",
        author_email="iacopo@domotz.com",
        description="Domotz Cloud stress test",
        license="Domotz ltd",
        install_requires=required_list,
        package_dir={"domotz_stress": "src/domotz_stress"},
        packages=packages,
        scripts=["bin/domotz_stress"],
    )
