from __future__ import unicode_literals
import hashlib
import json
import logging
import urlparse
import datetime

from tornado import gen
from tornado.gen import coroutine, Return
from tornado.httpclient import HTTPError, HTTPRequest

ACTIVATE_AGENT = '"ACTIVE"'
SUSPEND_AGENT = '"STANDBY"'

RETRIES = 100
TIMEOUT = 100.0

__author__ = 'Iacopo Papalini <iacopo@domotz.com>'
MACS = {}


class AgentAccount(object):
    def __init__(self, agent_id, mac, name, password, operation_mode):
        self.mac = mac
        self.id = agent_id
        self.name = name
        self.password = password
        self.operation_mode = operation_mode


class AgentCreator(object):
    def __init__(self, endpoint, secret, vendor, api_key, client):
        self.vendor = vendor
        self.endpoint = endpoint
        self.secret = secret
        self.api_key = api_key
        if self.endpoint[-1] != '/':
            self.endpoint += '/'
        self.client = client

    @coroutine
    def create_active_agent(self, agent_type, mac, callback=None):
        agent_account = None
        try:
            agent_account = yield self._try_hard_to_create_active_agent(agent_type, mac)
        finally:
            if callback:
                callback(agent_account)

    @coroutine
    def create_standby_agent(self, agent_type, mac, callback=None):
        agent_account = None
        try:
            agent_account = yield self._try_hard_to_create_standby_agent(agent_type, mac)
        finally:
            if callback:
                callback(agent_account)

    @coroutine
    def _log_and_wait(self, mac, e, attempts):
        sleeping = 5
        logging.error("Error creating agent {} ({}) attempts remaining: {} - sleeping {}s".format(mac,
                                                                                                  e.message,
                                                                                                  attempts,
                                                                                                  sleeping))
        yield gen.sleep(sleeping)

    @coroutine
    def _try_hard_to_create_active_agent(self, agent_type, mac):
        attempts = RETRIES
        standby_agent = None
        if mac in MACS:
            raise RuntimeError("Mac conflicting: {}".format(mac))
        MACS[mac] = 1
        while standby_agent is None and attempts > 0:
            logging.debug("Creating standby agent {}".format(mac))
            try:
                attempts -= 1
                standby_agent = yield self._create_standby_agent(agent_type, mac)
            except Exception, e:
                yield self._log_and_wait(mac, e, attempts)
        if not standby_agent:
            raise RuntimeError("Giving up")
        logging.debug("Created standby agent {}".format(mac))
        attempts = RETRIES

        success = False
        while not success and attempts > 0:
            try:
                attempts -= 1
                logging.debug("Activating agent {} {}".format(mac, standby_agent.id))
                success = yield self._user_activate_agent(standby_agent)
            except Exception, e:
                yield self._log_and_wait(mac, e, attempts)
        if not standby_agent:
            raise RuntimeError("Giving up")
        logging.debug("Activated agent {} {}".format(mac, standby_agent.id))
        attempts = RETRIES

        agent_account = None
        yield gen.sleep(0.5)
        while agent_account is None and attempts > 0:
            try:
                attempts -= 1
                logging.debug("Finishing activation agent {} {}".format(mac, standby_agent.id))
                agent_account = yield self._activate_agent(agent_type, mac)
                if agent_account.id != standby_agent.id:
                    raise RuntimeError('BINGO!!!')
            except Exception, e:
                yield self._log_and_wait(mac, e, attempts)
        if not agent_account:
            raise RuntimeError("Giving up")
        logging.debug("Finished activation agent {} {}".format(mac, standby_agent.id))

        raise Return(agent_account)

    @coroutine
    def _try_hard_to_create_standby_agent(self, agent_type, mac):
        attempts = RETRIES
        agent_account = None
        while agent_account is None and attempts > 0:
            try:
                attempts -= 1
                agent_account = yield self._create_standby_agent(agent_type, mac)
            except Exception, e:
                sleeping = 0.05 * (RETRIES - attempts)
                logging.error("Error creating agent {} ({}) attempts remaining: {} - sleeping {}s".format(mac,
                                                                                                          e.message,
                                                                                                          attempts,
                                                                                                          sleeping))
                yield gen.sleep(sleeping)

        raise Return(agent_account)

    @coroutine
    def _fetch_nonce(self):
        nonce_url = self.endpoint + 'agent-register-api/v1/nonce'
        try:
            response = yield self.client.fetch(nonce_url, headers={
                'X-Domotz-Agent-Vendor': self.vendor
            }, request_timeout=TIMEOUT, connect_timeout=TIMEOUT
                                               )
            nonce = json.loads(response.body)
            raise Return(nonce)
        except HTTPError, e:
            logging.error("Cannot fetch Nonce - {} - {}".format(e.response.code if e.response else '', e.message))
            return

    @coroutine
    def _create_standby_agent(self, agent_type, mac):
        nonce = yield self._fetch_nonce()
        response = yield self.client.fetch(self._build_create_request(agent_type, mac, nonce))
        location = response.headers['Location']
        agent_id = location.split('/')[-1]
        raw_url = urlparse.urlparse(location)

        raise Return(AgentAccount(agent_id=agent_id,
                                  password=raw_url.password,
                                  name=raw_url.username,
                                  mac=mac,
                                  operation_mode='STANDBY'))

    @coroutine
    def _activate_agent(self, agent_type, mac):
        nonce = yield self._fetch_nonce()
        response = yield self.client.fetch(self._build_create_request(agent_type, mac, nonce))
        location = response.headers['Location']
        agent_id = location.split('/')[-1]
        raw_url = urlparse.urlparse(location)

        raise Return(AgentAccount(agent_id=agent_id,
                                  password=raw_url.password,
                                  name=raw_url.username,
                                  mac=mac,
                                  operation_mode='ACTIVE'))

    @coroutine
    def _user_activate_agent(self, standby_agent):
        """
        :type standby_agent: domotz_stress.api.AgentAccount
        """
        activate_url = self.endpoint + 'enterprise-api/v1/agent/{}/operation_mode'.format(standby_agent.id)
        try:
            yield self.client.fetch(
                HTTPRequest(method='PUT', url=activate_url, body=(ACTIVATE_AGENT), headers={'X-Api-Key': self.api_key},
                            request_timeout=TIMEOUT, connect_timeout=TIMEOUT
                            )
            )
            raise Return(True)
        except HTTPError, e:
            if e.response and e.response.code == 409:
                logging.error("Conflict - re-activating agent {} {}".format(standby_agent.id, standby_agent.mac))
                yield self.client.fetch(
                    HTTPRequest(method='PUT', url=activate_url, body=(SUSPEND_AGENT),
                                headers={'X-Api-Key': self.api_key}, request_timeout=TIMEOUT, connect_timeout=TIMEOUT))
                yield self.client.fetch(
                    HTTPRequest(method='PUT', url=activate_url, body=(ACTIVATE_AGENT),
                                headers={'X-Api-Key': self.api_key}, request_timeout=TIMEOUT, connect_timeout=TIMEOUT))
            else:
                raise RuntimeError(
                    "Cannot activate agent {} - {} - {}".format(standby_agent.id, e.response.code if e.response else '',
                                                                e.message))

    def _build_create_request(self, agent_type, mac, nonce):
        create_url = self.endpoint + 'agent-register-api/v1/agent'
        fingerprint = "{}:{}".format(self.secret, nonce)
        fingerprint = hashlib.sha256(fingerprint).hexdigest()
        headers = {
            'X-Domotz-Agent-Vendor': self.vendor,
            'X-Registration-Nonce': nonce,
            'X-Registration-Fingerprint-Version': '1',
            'X-Registration-Fingerprint': fingerprint
        }
        data = {'mac_address': mac,
                'type': agent_type}
        body = json.dumps(data)
        create_request = HTTPRequest(method='POST', url=create_url, body=body, headers=headers, request_timeout=TIMEOUT,
                                     connect_timeout=TIMEOUT)
        return create_request
