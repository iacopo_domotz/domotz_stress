from __future__ import unicode_literals
from collections import deque
import logging
import random
import time
import datetime
import traceback

from tornado.gen import coroutine

from tornado.httpclient import AsyncHTTPClient

from tornado.ioloop import IOLoop

from domotz_stress.api import AgentCreator
from domotz_stress.utils import print_progress_bar
from domotz_stress.utils.simulator import AgentSimulator

__author__ = 'Iacopo Papalini <iacopo@domotz.com>'


class Statistics(object):
    def __init__(self, size):
        self.last_agent = None
        self.total = 0
        self.total_changed = 0
        self.duration = deque(maxlen=size)
        self.code = deque(maxlen=size)
        self.changed = deque(maxlen=size)

    def add_sample(self, agent_id, duration, code, changed):
        self.total += 1
        self.total_changed += changed
        self.last_agent = agent_id
        self.duration.append(duration)
        self.code.append(code)
        self.changed.append(changed)

    def as_string(self):
        _204 = len(filter(lambda x: int(x) == 204, self.code))
        _503 = len(filter(lambda x: int(x) == 503, self.code))
        _400 = len(filter(lambda x: int(x) == 400, self.code))
        return "Avg. duration:{:0.3f}s\t204:{}\t503:{}\t400:{}\tErros:{:0.2f}%\tChanged:{} (avg. {:0.2f})\tTotal calls:{}".format(
            reduce(lambda x, y: x + y, self.duration) / len(self.duration),
            _204,
            _503,
            _400,
            (_503 + _400 + 0.0) / len(self.code) * 100.0,
            self.total_changed,
            reduce(lambda x, y: x + y + 0.0, self.changed) / len(self.changed) * 1.0,
            self.total
        )


class StressApp(object):
    def __init__(self, arguments):
        self.arguments = arguments
        self.logger = logging
        self.io_loop = None
        AsyncHTTPClient.configure("tornado.curl_httpclient.CurlAsyncHTTPClient", max_clients=2000)
        self.client = AsyncHTTPClient()
        self.creator = AgentCreator(arguments.endpoint,
                                    arguments.secret,
                                    arguments.vendor_name,
                                    arguments.api_key,
                                    self.client)
        self.agents = {}
        self.pending_calls = 0
        self.start_time = None
        self.devices_number = arguments.devices_number
        self.device_status_change_percent = 100 - arguments.device_stability
        self.statistics = Statistics(arguments.devices_number * arguments.agent_number * 10)
        self.printing = False

    def start(self):
        current_ioloop = IOLoop.current()
        self.io_loop = current_ioloop

        current_ioloop.add_callback(self.main_loop)
        current_ioloop.start()

    @coroutine
    def main_loop(self):
        yield self.create_agents()

    @coroutine
    def create_agents(self):
        active_macs, standby_macs = self.generate_mac_addresses()
        total = len(active_macs) + len(standby_macs)
        print_progress_bar(0, total, prefix="Creating agents")
        self.start_time = time.time()
        self.pending_calls = total
        for iteration, mac in enumerate(active_macs):
            self.io_loop.add_timeout(
                datetime.timedelta(milliseconds=iteration * 500),
                self.creator.create_active_agent,
                self.arguments.agent_type, mac, self.add_agent)
        # for iteration, mac in enumerate(standby_macs, start=len(active_macs)):
        #     self.io_loop.add_timeout(
        #         datetime.timedelta(milliseconds=iteration * 500),
        #         self.creator.create_standby_agent,
        #         self.arguments.agent_type, mac, self.add_agent)

    def generate_mac_addresses(self):
        base = self.arguments.base_mac
        active_macs = []
        suspended_macs = []
        for i in xrange(self.arguments.agent_number):
            active_macs.append(base + ':{:02X}:{:02X}'.format(i // 256, i % 256))
        for i in xrange(self.arguments.agent_number, self.arguments.agent_number + self.arguments.standby_agent_number):
            suspended_macs.append(base + ':{:02X}:{:02X}'.format(i // 256, i % 256))
        test = set(active_macs)
        if len(test) != len(active_macs):
            raise RuntimeError("Conflicting mac addresses")

        return active_macs, suspended_macs

    @coroutine
    def simulate_discoveries(self):
        print "Simulating {} Agents, {} devices per ACTIVE agent".format(len(self.agents),
                                                                         self.devices_number)
        # Every agent has a period of 120s, every run every device has a 1% chance of changing status
        try:
            for agent in self.agents.values():
                delay_ms = random.randint(0, 120000)
                agent_simulator = AgentSimulator(agent,
                                                 self.arguments.endpoint,
                                                 self.arguments.devices_number,
                                                 self.arguments.device_stability,
                                                 self.client,
                                                 self.io_loop)
                self.io_loop.add_timeout(datetime.timedelta(milliseconds=delay_ms), agent_simulator.simulate,
                                         self.devices_defined,
                                         self.discovery_performed)
        except:
            print(traceback.format_exc())
        print ("All agents scheduled")

    def add_agent(self, agent):
        total = self.arguments.agent_number
        if agent:
            self.agents[agent.id] = agent
        self.pending_calls -= 1
        print_progress_bar(total - self.pending_calls, total, prefix="Creating agents")
        if self.pending_calls == 0:
            elapsed = time.time() - self.start_time
            print("{} agents created in {:0.3f}s".format(len(self.agents), elapsed))
            self.simulate_discoveries()
            self.pending_calls = total
            self.start_time = time.time()

    def devices_defined(self, _):
        total = self.arguments.agent_number
        self.pending_calls -= 1
        print_progress_bar(total - self.pending_calls, total, prefix="Defining devices")
        if self.pending_calls == 0:
            elapsed = time.time() - self.start_time
            print("{} devices created in {:0.3f}s".format(len(self.agents) * self.devices_number, elapsed))

    def discovery_performed(self, agent_id, changed, result, duration):
        self.statistics.add_sample(agent_id, duration, result, changed)
        if self.pending_calls == 0 and not self.printing:
            self.io_loop.add_timeout(datetime.timedelta(seconds=5), self.show_statistics)
            self.printing = True

    def show_statistics(self):
        print ("{}-{}".format(datetime.datetime.utcnow(), self.statistics.as_string()))
        self.io_loop.add_timeout(datetime.timedelta(seconds=5), self.show_statistics)
