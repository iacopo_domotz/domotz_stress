from __future__ import unicode_literals
import json
import logging
import datetime
import random
import time

from tornado import gen
from tornado.gen import coroutine
from tornado.httpclient import HTTPRequest, HTTPError

__author__ = 'Iacopo Papalini <iacopo@domotz.com>'
BASE_DEVICES_MAC = 'FF:EE:AA:'
DISCOVERY_PERIOD = 116
RETRIES = 100
TIMEOUT = 100.0


class SimulatedDevice(object):
    def __init__(self, mac, protocol='IP', online=True, ip=None, name=None):
        self.name = name
        self.ip = ip
        self.mac = mac
        self.protocol = protocol
        self.online = online
        self.id = None

    def define(self):
        ret = {
            'hw_address': self.mac,
            'protocol': self.protocol,
            'name': self.name
        }
        if self.protocol == 'IP' and self.ip:
            ret['ip_addresses'] = [self.ip]
        return ret

    def variables(self):
        return {
            'uid': self.id,
            'value': '1' if self.online else '0',
            'past': 0
        }

    def switch_status(self):
        self.online = not self.online
        return self


class AgentSimulator(object):
    def __init__(self, agent_account, endpoint, devices_number, device_stability, http_client, io_loop,
                 discovery_period_s=DISCOVERY_PERIOD):
        """
        :type agent_account: domotz_stress.api.AgentAccount
        """
        self.io_loop = io_loop
        self.http_client = http_client
        self.agent_account = agent_account
        self.endpoint = endpoint
        self.device_stability = device_stability
        self.discovery_period_s = discovery_period_s
        if self.endpoint[-1] != '/':
            self.endpoint += '/'

        self.devices = {}
        for i in xrange(devices_number):
            mac = BASE_DEVICES_MAC + ':{:02X}:{:02X}'.format(i // 256, i % 256)
            ip = "192.168.1.{}".format(i + 2)
            name = "Device {}-{}".format(agent_account.id, i + 1)
            self.devices[mac] = SimulatedDevice(mac, 'IP', True, ip, name)

    def devices_definition(self):
        return [device.define() for device in self.devices.values()]

    @coroutine
    def simulate(self, definitions_callback, discovery_callback):
        if self.agent_account.operation_mode == 'ACTIVE':
            yield self._define_devices(definitions_callback)
            yield self._discovery(discovery_callback)
        else:
            definitions_callback(0)
            # yield self._heartbeat()

    @coroutine
    def _define_devices(self, definitions_callback):
        url = self.endpoint + 'agent-api/v1/agent/{}/device'.format(self.agent_account.id)
        try:
            attempts = RETRIES
            ok = False
            request = HTTPRequest(method='PUT',
                                  url=url,
                                  body=json.dumps(self.devices_definition()),
                                  auth_username=self.agent_account.name,
                                  auth_password=self.agent_account.password,
                                  request_timeout=TIMEOUT, connect_timeout=TIMEOUT)
            while not ok and attempts > 0:
                try:
                    attempts -= 1
                    yield self.http_client.fetch(request)
                    ok = True
                except HTTPError, e:
                    if attempts > 0:
                        yield gen.sleep(5)
                        continue
                    logging.error(
                        "Cannot define devices for agent {}: {} {}".format(self.agent_account.id, e.code, e.message))
                    raise e

            request = HTTPRequest(method='GET',
                                  url=url,
                                  auth_username=self.agent_account.name,
                                  auth_password=self.agent_account.password,
                                  request_timeout=TIMEOUT, connect_timeout=TIMEOUT)
            attempts = RETRIES
            ok = False
            while not ok and attempts > 0:
                try:
                    attempts -= 1
                    response = yield self.http_client.fetch(request)
                    defined_devices = json.loads(response.body)
                    for defined_device in defined_devices:
                        self.devices[defined_device['hw_address']].id = defined_device['id']
                    ok = all([device.id for device in self.devices.values()])
                    if not ok:
                        raise HTTPError("Missing devices")
                except HTTPError, e:
                    if attempts > 0:
                        yield gen.sleep(5)
                        continue
                    logging.error(
                        "Cannot fetch devices for agent {}: {} {}".format(self.agent_account.id, e.code, e.message))
                    raise e

            if not ok:
                logging.error(
                    "Cannot fetch defined devices for agent {}".format(self.agent_account.id))
        finally:
            definitions_callback(len(self.devices))

    @coroutine
    def _discovery(self, discovery_callback):
        result = None
        changed = []
        start = time.time()
        try:
            for device in self.devices.values():
                if random.randint(1, 100) > self.device_stability:
                    changed.append(device.switch_status())
            url = self.endpoint + 'agent-api/v1/agent/{}/variable-value'.format(self.agent_account.id)
            request = HTTPRequest(method='POST',
                                  url=url,
                                  body=json.dumps([device.variables() for device in changed]),
                                  auth_username=self.agent_account.name,
                                  auth_password=self.agent_account.password,
                                  request_timeout=TIMEOUT, connect_timeout=TIMEOUT)
            total = len(changed)
            up = len(filter(lambda device: device.online, changed))
            down = len(filter(lambda device: not device.online, changed))
            logging.info("{} changed devices for agent {} - up: {},down: {}".format(total, self.agent_account.id,
                                                                                    up, down))

            start = time.time()
            response = yield self.http_client.fetch(request)
            result = response.code
        except HTTPError, e:
            result = e.response.code if e.response else '499'
        finally:
            duration = time.time() - start
            discovery_callback(self.agent_account.id, len(changed), result, duration)
            self.io_loop.add_timeout(datetime.timedelta(seconds=self.discovery_period_s),
                                     self._discovery,
                                     discovery_callback)
